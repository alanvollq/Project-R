namespace Core.Managers.UI.Popup_Manager
{
    public interface IPopupManager : IManager
    {
        public void Open<TPopup>() where TPopup : BasePopup;
        public void CloseLast();
        public void CloseAll();
    }
}