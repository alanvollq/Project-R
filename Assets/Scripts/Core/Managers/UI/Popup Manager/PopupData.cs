using System.Collections.Generic;
using UnityEngine;

namespace Core.Managers.UI.Popup_Manager
{
    [CreateAssetMenu(fileName = "Popup Data", menuName = "Data/Managers/UI/Popup Manager/Popup Data")]
    public class PopupData : ScriptableObject
    {
        [SerializeField] private List<BasePopup> _popups;

        
        public IEnumerable<BasePopup> Popups => _popups;
    }
}