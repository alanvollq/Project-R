using UnityEngine;

namespace Core.Managers.UI.Popup_Manager
{
    public abstract class BasePopup : MonoBehaviour
    {
        [SerializeField] private GameObject _content;
        [SerializeField] private bool _openingIsSingle;
        [SerializeField] private bool _hidePrev;

        
        private bool _isShowed;
        
        
        public bool OpeningIsSingle => _openingIsSingle;

        public bool HidePrev => _hidePrev;
        
        public bool IsShowed => _isShowed;


        public BasePopup Initialization()
        {
            OnInitialization();
            gameObject.SetActive(false);

            return this;
        }

        protected virtual void OnInitialization() { }
        
        public void Open()
        {
            OnBeforeOpen();
            gameObject.SetActive(true);
            OnAfterOpen();
            Show();
        }

        public void Close()
        {
            Hide();
            OnBeforeClose();
            gameObject.SetActive(false);
            OnAfterClose();
        }

        public void Show()
        {
            _content.SetActive(true);
            _isShowed = true;
        }

        public void Hide()
        {
            _content.SetActive(false);
            _isShowed = false;
        }

        protected virtual void OnBeforeOpen()
        {
        }

        protected virtual void OnAfterOpen()
        {
        }

        protected virtual void OnBeforeClose()
        {
        }

        protected virtual void OnAfterClose()
        {
        }

        protected virtual void OnBeforeShow()
        {
        }

        protected virtual void OnAfterShow()
        {
        }

        protected virtual void OnBeforeHide()
        {
        }

        protected virtual void OnAfterHide()
        {
        }
    }
}