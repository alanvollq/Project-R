using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static UnityEngine.Object;

namespace Core.Managers.UI.Popup_Manager
{
    public sealed class PopupManager : IPopupManager
    {
        private readonly PopupData _popupData;
        private Dictionary<Type, BasePopup> _popups;
        private readonly Stack<BasePopup> _openedPopups = new();


        public PopupManager(PopupData popupData)
        {
            _popupData = popupData;
        }


        public void Initialization(CoreRoot coreRoot)
        {
            var popupsParent = new GameObject("[ Popups ]");
            popupsParent.transform.SetParent(coreRoot.UIRoot.transform);

            _popups = _popupData.Popups
                .Select(popup => Instantiate(popup, popupsParent.transform).Initialization())
                .ToDictionary(popup => popup.GetType());
        }

        public void Open<TPopup>() where TPopup : BasePopup
        {
            var key = typeof(TPopup);
            _popups.TryGetValue(key, out var openingPopup);
            if (openingPopup == null)
                throw new Exception($"Popup [{key}] is not found!!!");

            if (openingPopup.OpeningIsSingle && _openedPopups.Count > 0)
                CloseAll();

            if (openingPopup.HidePrev && _openedPopups.Count > 0)
                _openedPopups.Peek().Hide();

            openingPopup.Open();
            _openedPopups.Push(openingPopup);
        }

        public void CloseLast()
        {
            if (_openedPopups.Count > 0)
                _openedPopups.Pop().Close();

            if (_openedPopups.Count <= 0)
                return;

            if (_openedPopups.Peek().IsShowed)
                return;

            _openedPopups.Peek().Show();
        }

        public void CloseAll()
        {
            while (_openedPopups.Count > 0)
                _openedPopups.Pop().Close();
        }
    }
}