using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static UnityEngine.Object;

namespace Core.Managers.UI.MessageBox_Manager
{
    public sealed class MessageBoxManager : IMessageBoxManager
    {
        private readonly MessageBoxData _messageBoxData;
        private Dictionary<Type, BaseMessageBox> _messageBoxes;

        
        public MessageBoxManager(MessageBoxData messageBoxData)
        {
            _messageBoxData = messageBoxData;
        }

        
        public void Initialization(CoreRoot coreRoot)
        {
            var messageBoxesParent = new GameObject("[ MessageBoxes ]");
            messageBoxesParent.transform.SetParent(coreRoot.UIRoot.transform);

            _messageBoxes = _messageBoxData.MessageBoxes
                .Select(messageBox => Instantiate(messageBox, messageBoxesParent.transform).Initialization())
                .ToDictionary(messageBox => messageBox.GetType());
        }

        public void Open<TMessageBox>(Action okAction, Action cancelAction) where TMessageBox : BaseMessageBox
        {
            
        }
    }
}