using System;

namespace Core.Managers.UI.MessageBox_Manager
{
    public interface IMessageBoxManager : IManager
    {
        public void Open<TMessageBox>(Action okAction, Action cancelAction) where TMessageBox : BaseMessageBox;
    }
}