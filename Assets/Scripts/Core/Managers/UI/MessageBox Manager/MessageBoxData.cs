using System.Collections.Generic;
using UnityEngine;

namespace Core.Managers.UI.MessageBox_Manager
{
    [CreateAssetMenu(fileName = "MessageBox Data", menuName = "Data/Managers/UI/MessageBox Manager/MessageBox Data")]
    public class MessageBoxData : ScriptableObject
    {
        [SerializeField] private List<BaseMessageBox> _messageBoxes;


        public IEnumerable<BaseMessageBox> MessageBoxes => _messageBoxes;
    }
}