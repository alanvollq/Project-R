using UnityEngine;

namespace Core.Managers.UI.MessageBox_Manager
{
    public abstract class BaseMessageBox : MonoBehaviour
    {

        public BaseMessageBox Initialization()
        {
            OnInitialization();
            gameObject.SetActive(false);
            
            return this;
        }

        protected virtual void OnInitialization() { }
    }
}