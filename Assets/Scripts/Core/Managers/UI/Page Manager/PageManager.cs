using System;
using System.Collections.Generic;
using System.Linq;
using Core.Managers.UI.Popup_Manager;
using UnityEngine;
using static UnityEngine.Object;

namespace Core.Managers.UI.Page_Manager
{
    public sealed class PageManager : IPageManager
    {
        private readonly PageData _pageData;
        private IDictionary<Type, BasePage> _pages;
        private BasePage _openedPage;
        private IPopupManager _popupManager;


        public PageManager(PageData pageData)
        {
            _pageData = pageData;
        }


        public void Initialization(CoreRoot coreRoot)
        {
            var pagesParent = new GameObject("[ Pages ]");
            pagesParent.transform.SetParent(coreRoot.UIRoot);
            
            _pages = _pageData.Pages
                .Select(basePage => Instantiate(basePage, pagesParent.transform).Initialization())
                .ToDictionary(basePage => basePage.GetType());

            _popupManager = ManagerProvider.GetManager<IPopupManager>();
        }

        public void Open<TPage>() where TPage : BasePage
        {
            var type = typeof(TPage);
            if (!_pages.TryGetValue(type, out var openingPage))
                return;

            CloseLast();
            openingPage.Open();
            _openedPage = openingPage;
        }

        private void CloseLast()
        {
            _popupManager.CloseAll();
            
            if (_openedPage != null)
                _openedPage.Close();
        }
    }
}