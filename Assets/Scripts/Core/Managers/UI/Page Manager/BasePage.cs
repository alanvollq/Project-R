using UnityEngine;

namespace Core.Managers.UI.Page_Manager
{
    public abstract class BasePage : MonoBehaviour
    {
        public BasePage Initialization()
        {
            OnInitialization();
            gameObject.SetActive(false);
            
            return this;
        }

        protected virtual void OnInitialization() { }
        
        public void Open()
        {
            OnBeforeOpen();
            gameObject.SetActive(true);
            OnAfterOpen();
        }
        
        public void Close()
        {
            OnBeforeClose();
            gameObject.SetActive(false);
            OnAfterClose();
        }

        protected virtual void OnBeforeOpen()
        {
        }

        protected virtual void OnAfterOpen()
        {
        }

        protected virtual void OnBeforeClose() {
        }

        protected virtual void OnAfterClose() { }
    }
}