namespace Core.Managers.UI.Page_Manager
{
    public interface IPageManager : IManager
    {
        public void Open<TPage>() where TPage : BasePage;
    }
}