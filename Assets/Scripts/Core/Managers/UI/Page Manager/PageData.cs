using System.Collections.Generic;
using UnityEngine;

namespace Core.Managers.UI.Page_Manager
{
    [CreateAssetMenu(fileName = "Page Data", menuName = "Data/Managers/UI/Page Manager/Page Data")]
    public class PageData : ScriptableObject
    {
        [SerializeField] private List<BasePage> _pages;


        public IEnumerable<BasePage> Pages => _pages;
    }
}