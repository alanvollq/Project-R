namespace Core.Managers
{
    public interface IManager
    {
        public void Initialization(CoreRoot coreRoot);
    }
}