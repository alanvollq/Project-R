using System;
using System.Collections.Generic;
using Core.Managers;

namespace Core
{
    public static class ManagerProvider
    {
        private static readonly Dictionary<Type, IManager> _managers = new();


        public static void Initialize(CoreRoot coreRoot)
        {
            foreach (var manager in _managers.Values)
                manager.Initialization(coreRoot);
        }

        public static void AddManager<TManager>(TManager manager) where TManager : IManager
        {
            var key = typeof(TManager);
            if (_managers.ContainsKey(key))
                throw new Exception($"Manager {key} is exist");

            _managers.Add(key, manager);
        }

        public static TManager GetManager<TManager>() where TManager : IManager
        {
            var key = typeof(TManager);
            if (!_managers.TryGetValue(key, out var manager))
                throw new Exception($"Manager {key} is not exist");

            return (TManager)manager;
        }
    }
}