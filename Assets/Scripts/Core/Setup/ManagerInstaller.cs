using Core.Managers.UI.MessageBox_Manager;
using Core.Managers.UI.Page_Manager;
using Core.Managers.UI.Popup_Manager;
using UI.Pages;
using UnityEngine;

namespace Core.Setup
{
    public class ManagerInstaller : MonoBehaviour
    {
        [SerializeField] private CoreRoot _coreRoot;
        [SerializeField] private ManagersData _managersData;

        private PageManager _pageManager;
        

        private void Awake()
        {
            SetupManagers();
            DontDestroyOnLoad(_coreRoot.gameObject);
            Destroy(gameObject);
        }

        private void SetupManagers()
        {
            CreateManagers(_managersData);
            ManagerProvider.Initialize(_coreRoot);
            
            _pageManager.Open<UIMainPage>();
        }

        private void CreateManagers(ManagersData data)
        {
            var pageManager = new PageManager(data.UI.PageData);
            var popupManager = new PopupManager(data.UI.PopupData);
            var messageBoxManager = new MessageBoxManager(data.UI.MessageBoxData);

            ManagerProvider.AddManager<IPageManager>(pageManager);
            ManagerProvider.AddManager<IPopupManager>(popupManager);
            ManagerProvider.AddManager<IMessageBoxManager>(messageBoxManager);

            _pageManager = pageManager;
        }
    }
}