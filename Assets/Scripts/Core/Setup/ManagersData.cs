using System;
using Core.Managers.UI.MessageBox_Manager;
using Core.Managers.UI.Page_Manager;
using Core.Managers.UI.Popup_Manager;
using UnityEngine;

namespace Core.Setup
{
    [CreateAssetMenu(fileName = "Managers Data", menuName = "Data/Managers/Managers Data")]
    public class ManagersData : ScriptableObject
    {
        [SerializeField] private UIData _uiData;


        public UIData UI => _uiData;
    }


    [Serializable]
    public sealed class UIData
    {
        [SerializeField] private PageData _pageData;
        [SerializeField] private PopupData _popupData;
        [SerializeField] private MessageBoxData _messageBoxData;


        public PageData PageData => _pageData;
        public PopupData PopupData => _popupData;
        public MessageBoxData MessageBoxData => _messageBoxData;
    }
}