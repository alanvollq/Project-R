using UnityEngine;

namespace Core
{
    public class CoreRoot : MonoBehaviour
    {
        [SerializeField] private Transform _uiRoot;
        [SerializeField] private Transform _gameRoot;

        
        public Transform UIRoot => _uiRoot;
        public Transform GameRoot => _gameRoot;
    }
}