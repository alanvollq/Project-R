using Core;
using Core.Managers.UI.Page_Manager;
using UI.Pages;
using UnityEngine;

namespace Misc
{
    public class GameController : MonoBehaviour
    {
        private IPageManager _pageManager;

        private void Awake()
        {
          _pageManager =  ManagerProvider.GetManager<IPageManager>();
          _pageManager.Open<UIMainPage>();
        }
    }
}